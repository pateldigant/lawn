import re
import random
import numpy as np
import os.path
import scipy.misc
import shutil
import zipfile
import time
import tensorflow as tf
from glob import glob
from urllib.request import urlretrieve
from tqdm import tqdm


class DLProgress(tqdm):
    last_block = 0

    def hook(self, block_num=1, block_size=1, total_size=None):
        self.total = total_size
        self.update((block_num - self.last_block) * block_size)
        self.last_block = block_num


def maybe_download_pretrained_vgg(data_dir):
    """
    Download and extract pretrained vgg model if it doesn't exist
    :param data_dir: Directory to download the model to
    """
    vgg_filename = 'vgg.zip'
    vgg_path = os.path.join(data_dir, 'vgg')
    vgg_files = [
        os.path.join(vgg_path, 'variables/variables.data-00000-of-00001'),
        os.path.join(vgg_path, 'variables/variables.index'),
        os.path.join(vgg_path, 'saved_model.pb')]

    missing_vgg_files = [vgg_file for vgg_file in vgg_files if not os.path.exists(vgg_file)]
    if missing_vgg_files:
        # Clean vgg dir
        if os.path.exists(vgg_path):
            shutil.rmtree(vgg_path)
        os.makedirs(vgg_path)

        # Download vgg
        print('Downloading pre-trained vgg model...')
        with DLProgress(unit='B', unit_scale=True, miniters=1) as pbar:
            urlretrieve(
                'https://s3-us-west-1.amazonaws.com/udacity-selfdrivingcar/vgg.zip',
                os.path.join(vgg_path, vgg_filename),
                pbar.hook)

        # Extract vgg
        print('Extracting model...')
        zip_ref = zipfile.ZipFile(os.path.join(vgg_path, vgg_filename), 'r')
        zip_ref.extractall(data_dir)
        zip_ref.close()

        # Remove zip file to save space
        os.remove(os.path.join(vgg_path, vgg_filename))


def gen_batch_function(data_folder, image_shape):
    """
    Generate function to create batches of training data
    :param data_folder: Path to folder that contains all the datasets
    :param image_shape: Tuple - Shape of image
    :return:
    """
    def get_batches_fn(batch_size):
        """
        Create batches of training data
        :param batch_size: Batch Size
        :return: Batches of training data
        """
        image_paths = glob(os.path.join(data_folder, 'grass', '*.jpg'))
        label_paths = {
            os.path.basename(path)[:os.path.basename(path).rfind('_')]+'.jpg': path
            for path in glob(os.path.join('ground_truth', '*_mask.jpg'))}
        #bgr
        background_color = np.array([0, 0, 0])
        grass_color = np.array([254,0,0])
        mud_color = np.array([0,255,1])
        tree_color = np.array([0,0,254])
        pipe_color = np.array([255,255,0])
        gutter_color = np.array([0,255,255])
        fence_color = np.array([255,0,254])

        random.shuffle(image_paths)
        for batch_i in range(0, len(image_paths), batch_size):
            images = []
            gt_images = []
            for image_file in image_paths[batch_i:batch_i+batch_size]:
                gt_image_file = label_paths[os.path.basename(image_file)]

                image = scipy.misc.imresize(scipy.misc.imread(image_file), image_shape)
                gt_image = scipy.misc.imresize(scipy.misc.imread(gt_image_file), image_shape)

                #gt_bg = np.all(gt_image == background_color, axis=2)
                gt_bg = np.all((gt_image == background_color) | (gt_image != grass_color), axis=2)
                gt_bg = gt_bg.reshape(*gt_bg.shape, 1)
                
                gt_grass = np.all(gt_image == grass_color, axis=2)
                gt_grass = gt_grass.reshape(*gt_grass.shape, 1)
                gt_image = np.concatenate((gt_bg, gt_grass),axis=2)
'''
                gt_mud = np.all(gt_image == mud_color, axis=2)
                gt_mud = gt_mud.reshape(*gt_mud.shape, 1)
                
                gt_tree = np.all(gt_image == tree_color, axis=2)
                gt_tree = gt_tree.reshape(*gt_tree.shape, 1)

                gt_pipe = np.all(gt_image == pipe_color, axis=2)
                gt_pipe = gt_pipe.reshape(*gt_pipe.shape, 1)

                gt_gutter = np.all(gt_image == gutter_color, axis=2)
                gt_gutter = gt_gutter.reshape(*gt_gutter.shape, 1)

                gt_fence = np.all(gt_image == fence_color, axis=2)
                gt_fence = gt_fence.reshape(*gt_fence.shape, 1)
  '''              
                #gt_image = np.concatenate((gt_bg, gt_grass, gt_mud, gt_tree, gt_pipe, gt_gutter, gt_fence), axis=2)

                images.append(image)
                gt_images.append(gt_image)

            yield np.array(images), np.array(gt_images)
    return get_batches_fn


def gen_test_output(sess, logits, keep_prob, image_pl, data_folder, image_shape):
    """
    Generate test output using the test images
    :param sess: TF session
    :param logits: TF Tensor for the logits
    :param keep_prob: TF Placeholder for the dropout keep robability
    :param image_pl: TF Placeholder for the image placeholder
    :param data_folder: Path to the folder that contains the datasets
    :param image_shape: Tuple - Shape of image
    :return: Output for for each test image
    """
    for image_file in glob(os.path.join(data_folder,  '*.jpg')):
        image = scipy.misc.imresize(scipy.misc.imread(image_file), image_shape)
        masked_im = scipy.misc.toimage(image)
        im_softmax = sess.run(
            [tf.nn.softmax(logits)],
            {keep_prob: 1.0, image_pl: [image]})
        #grass
        im_softmax_g = im_softmax[0][:, 1].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax_g > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[191, 63, 127, 64]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")
        masked_im.paste(mask, box=None, mask=mask)
        '''
        #mud
        im_softmax_m = im_softmax[0][:, 2].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax_m > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[225, 225, 29, 64]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")
        masked_im.paste(mask, box=None, mask=mask)
        #tree
        im_softmax_t = im_softmax[0][:, 3].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax_t > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[90, 48, 6, 64]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")
        masked_im.paste(mask, box=None, mask=mask)
        #pipe
        im_softmax_p = im_softmax[0][:, 4].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax_p > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[6, 4, 133, 64]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")
        masked_im.paste(mask, box=None, mask=mask)
        #gutter
        im_softmax_gt = im_softmax[0][:, 5].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax_gt > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[22, 252, 248, 64]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")
        masked_im.paste(mask, box=None, mask=mask)
        #fence
        im_softmax_f = im_softmax[0][:, 6].reshape(image_shape[0], image_shape[1])
        segmentation = (im_softmax_f > 0.5).reshape(image_shape[0], image_shape[1], 1)
        mask = np.dot(segmentation, np.array([[185, 96, 8, 64]]))
        mask = scipy.misc.toimage(mask, mode="RGBA")
        masked_im.paste(mask, box=None, mask=mask)
        '''
        
        yield os.path.basename(image_file), np.array(masked_im)


def save_inference_samples(runs_dir, data_dir, sess, image_shape, logits, keep_prob, input_image):
    # Make folder for current run
    output_dir = os.path.join(runs_dir, str(time.time()))
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)

    # Run NN on test images and save them to HD
    print('Training Finished. Saving test images to: {}'.format(output_dir))
    image_outputs = gen_test_output(
        sess, logits, keep_prob, input_image, os.path.join(data_dir, 'test'), image_shape)
    for name, image in image_outputs:
        scipy.misc.imsave(os.path.join(output_dir, name), image)